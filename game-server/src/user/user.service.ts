import { Injectable } from '@nestjs/common'
import { db } from 'src/db'

@Injectable()
export class UserService {
  login(user: { username: string }) {
    let id = db.queryFirstCell('select id from user where username = ?', [
      user.username,
    ])
    if (!id) {
      id = db.run('insert into user (username) values (?)', [
        user.username,
      ]).lastInsertRowid
    }
    return { id }
  }
}
