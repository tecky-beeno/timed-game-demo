import { Body, Controller, Post } from '@nestjs/common'
import { object, string } from 'cast.ts'
import { UserService } from './user.service'
import { TokenService } from 'src/token/token.service'

@Controller('user')
export class UserController {
  constructor(
    private userService: UserService,
    private tokenService: TokenService,
  ) {}

  @Post('/login')
  login(@Body() body) {
    let parser = object({
      username: string({ minLength: 3 }),
    })
    let user = parser.parse(body)
    let { id } = this.userService.login(user)
    let token = this.tokenService.encode({ id })
    return { token }
  }
}
