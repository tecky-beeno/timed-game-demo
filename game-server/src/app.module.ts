import { Module } from '@nestjs/common'
import { ServeStaticModule } from '@nestjs/serve-static'
import { AppController } from './app.controller'
import { AppService } from './app.service'
import { UserController } from './user/user.controller'
import { UserService } from './user/user.service'
import { TokenService } from './token/token.service';
import { GameService } from './game/game.service';
import { GameController } from './game/game.controller';

@Module({
  imports: [
    ServeStaticModule.forRoot({
      rootPath: '../game-app/www',
    }),
  ],
  controllers: [AppController, UserController, GameController],
  providers: [AppService, UserService, TokenService, GameService],
})
export class AppModule {}
