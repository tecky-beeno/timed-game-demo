import { Injectable } from '@nestjs/common'
import { db } from 'src/db'

@Injectable()
export class GameService {
  addRecord(record: { user_id: number; score: number }) {
    let id = db.insert('game', record)
    return { id }
  }
}
