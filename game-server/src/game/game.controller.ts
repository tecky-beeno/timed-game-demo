import { Body, Controller, Header, Headers, Post } from '@nestjs/common'
import { int, object } from 'cast.ts'
import { TokenService } from 'src/token/token.service'
import { GameService } from './game.service'

@Controller('game')
export class GameController {
  constructor(
    private gameService: GameService,
    private tokenService: TokenService,
  ) {}

  @Post()
  submit(@Body() body, @Headers('Authorization') authorization) {
    let { id: user_id } = this.tokenService.parseFromHeader(authorization)
    let parser = object({
      score: int({ min: 0 }),
    })
    let { score } = parser.parse(body)
    return this.gameService.addRecord({ user_id, score })
  }
}
