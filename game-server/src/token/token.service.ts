import { Injectable, HttpException, HttpStatus } from '@nestjs/common'
import { encode, decode } from 'jwt-simple'
import { env } from 'src/env'
import { JWTPayload } from './jwt-payload'

@Injectable()
export class TokenService {
  encode(payload: JWTPayload): string {
    return encode(payload, env.JWT_SECRET)
  }

  decode(token: string): JWTPayload {
    return decode(token, env.JWT_SECRET)
  }

  parseFromHeader(authorization: string) {
    let token = authorization?.split('Bearer ')[1]
    if (!token) {
      throw new HttpException('Missing JWT Token', HttpStatus.UNAUTHORIZED)
    }
    return this.decode(token)
  }
}
