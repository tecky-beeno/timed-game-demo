# Visualize on https://erd.surge.sh
# or https://quick-erd.surge.sh
#
# Relationship Types
#  -    - one to one
#  -<   - one to many
#  >-   - many to one
#  >-<  - many to many
#  -0   - one to zero or one
#  0-   - zero or one to one
#  0-0  - zero or one to zero or one
#  -0<  - one to zero or many
#  >0-  - zero or many to one
#
////////////////////////////////////


user
----
id integer PK
username text


game
----
id integer PK
user_id integer FK >- user.id
score integer


# user (97, 82)
# game (373, 130)
