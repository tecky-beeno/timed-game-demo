import { Component } from '@angular/core'
import { UserService } from '../user.service'

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss'],
})
export class Tab1Page {
  user = { username: '' }
  constructor(private userService: UserService) {}
  login() {
    this.userService.login(this.user)
  }
}
