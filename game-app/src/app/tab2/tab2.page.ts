import { Component } from '@angular/core'
import { Router } from '@angular/router'
import { GameService } from '../game.service'

@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss'],
})
export class Tab2Page {
  isStarted = false

  score = 0
  timer = 0

  constructor(private gameService: GameService) {}

  start() {
    this.isStarted = true
    this.timer = 5
    this.score = 0
    let timer = setInterval(() => {
      if (this.timer === 0) {
        clearInterval(timer)
        this.end()
        return
      }
      this.timer--
    }, 1000)
  }

  end() {
    this.isStarted = false
    this.gameService.submit({ score: this.score })
  }

  checkRound() {
    this.score++
  }
}
