import { Injectable } from '@angular/core'
import { object } from 'cast.ts'
import { ApiService } from './api.service'

@Injectable({
  providedIn: 'root',
})
export class GameService {
  constructor(private api: ApiService) {}

  submit(input: { score: number }) {
    let parser = object({})
    this.api.post({
      url: '/game',
      body: input,
      parser,
      successMessage: 'Submitted game record',
    })
  }
}
