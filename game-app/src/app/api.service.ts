import { Injectable } from '@angular/core'
import { ToastController } from '@ionic/angular'
import { Parser } from 'cast.ts'

@Injectable({
  providedIn: 'root',
})
export class ApiService {
  api_origin = 'http://localhost:3000'

  private token = localStorage.getItem('token')

  constructor(private toastController: ToastController) {}

  setToken(token: string) {
    this.token = token
    localStorage.setItem('token', token)
  }

  private fetch<T>(
    url: string,
    init: RequestInit,
    parser: Parser<T>,
  ): Promise<T> {
    let p = fetch(this.api_origin + url, {
      ...init,
      headers: {
        ...init.headers,
        Authorization: 'Bearer ' + this.token,
      },
    }).then(res =>
      res.json().then(json => {
        if (200 <= res.status && res.status <= 299) {
          if (json.error) {
            throw json.error
          }
          return parser.parse(json, {})
        } else {
          throw json.message
        }
      }),
    )
    p.catch(async error => {
      this.showToast({
        message: String(error),
        color: 'danger',
        duration: 3500,
      })
    })
    return p
  }

  private async showToast(options: {
    message: string
    color: string
    duration: number
  }) {
    let toast: HTMLIonToastElement = await this.toastController.create({
      color: options.color,
      duration: options.duration,
      message: options.message,
      buttons: [
        { text: 'Dismiss', role: 'cancel', handler: () => toast.dismiss() },
      ],
    })
    await toast.present()
  }

  async post<T>(input: {
    successMessage: string
    url: string
    body: object
    parser: Parser<T>
  }) {
    let json = await this.fetch(
      input.url,
      {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(input.body),
      },
      input.parser,
    )
    this.showToast({
      message: input.successMessage,
      duration: 3000,
      color: 'success',
    })
    return json
  }
}
