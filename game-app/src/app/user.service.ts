import { Injectable } from '@angular/core'
import { Router } from '@angular/router'
import { ToastController } from '@ionic/angular'
import { object, optional, string } from 'cast.ts'
import { ApiService } from './api.service'

@Injectable({
  providedIn: 'root',
})
export class UserService {
  constructor(private api: ApiService, private router: Router) {}

  async login(user: { username: string }) {
    let parser = object({
      token: string(),
    })
    let json = await this.api.post({
      url: '/user/login',
      body: user,
      parser,
      successMessage: 'Login successfully',
    })
    this.api.setToken(json.token)
    this.router.navigateByUrl('/tabs/tab2')
  }
}
